#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from RM6785 device
$(call inherit-product, device/realme/RM6785/device.mk)

PRODUCT_DEVICE := RM6785
PRODUCT_NAME := omni_RM6785
PRODUCT_BRAND := realme
PRODUCT_MODEL := RM6785
PRODUCT_MANUFACTURER := realme

PRODUCT_GMS_CLIENTID_BASE := android-oppo

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="bootleg_RM6785-userdebug 13 TQ1A.230205.002 eng.hansra.20230222.165113 release-keys"

BUILD_FINGERPRINT := realme/bootleg_RM6785/RM6785:13/TQ1A.230205.002/hansraj02221430:userdebug/release-keys
